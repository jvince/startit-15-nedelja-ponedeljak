var Helpers = {
	/**
	 * Kreira XMLHttp poziv
	 *
	 * @argument {string} type Tip zahteva (GET, POST)
	 * @argument {string} url Lokacija
	 * @argument {any} data Podaci za slanje
	 * @argument {object} callbacks Povratni pozivi
	 * @argument {object} headers Zaglavlja
	 *
	 * @returns {undefined}
	 */
	createRequest: function (type, url, data, callbacks, headers) {
		callbacks = callbacks || {};
		headers = headers || {};
		data = data === null || data === undefined
			? '' : JSON.stringify(data);

		var httpRequest = new XMLHttpRequest();
		httpRequest.open(type, url, true);

		for (var header in headers) {
			httpRequest.setRequestHeader(
				header, headers[header]
			);
		}

		for (var event in callbacks) {
			httpRequest.addEventListener(event, function () {
				callbacks[event].call(httpRequest, httpRequest.response);
			});
		}

		httpRequest.send(data);
	},
	/**
	 * Kreira jsonp zahtev
	 *
	 * @argument {string} url Lokacija
	 * @argument {object} data Podaci za slanje
	 * @argument {string} callback Naziv jsonp funkcije
	 * @argument {Function} onComplete Povratna fukcija za prihvat rezultata
	 */
	jsonp: function (url, data, callback, onComplete) {
		data = data || {};
		callback = callback || 'callback';
		onComplete = onComplete || function () {};

		var queryParams = [];
		var script = document.createElement('script');

		for (var key in data) {
			queryParams.push(
				encodeURIComponent(key) + '=' + encodeURIComponent(data[key])
			);
		}

		queryParams.push('callback=' + callback);
		url = url + '?' + queryParams.join('&');

		script.type = 'application/javascript';
		script.src = url;

		script.addEventListener('load', function () {
			var script = document.head.querySelector('[src="' + url + '"]');
			if (script) {
				script.parentNode.removeChild(script);
			}
		});

		window[callback] = function (data) {
			delete window[callback];
			onComplete(data);
		}

		document.head.appendChild(script);
	}
};