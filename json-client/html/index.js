var $json = document.getElementById('json');
var $jsonp = document.getElementById('jsonp');

$json.addEventListener('click', function () {
	Helpers.createRequest('GET', 'http://localhost:8001/json.php', null,
		{
			'loadend': function (response) {
				try {
					console.log(JSON.parse(response))
				}
				catch (err) {
					console.log(err.message)
				}
			}
		}
	);
});

$jsonp.addEventListener('click', function () {
	Helpers.jsonp('http://localhost:8001/jsonp.php', { n: 1 }, 'cb', function (data) {
		console.log(data)
	});
});