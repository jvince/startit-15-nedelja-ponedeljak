import Hapi from 'hapi';

const server = Hapi.server({
	port: 8080,
	host: 'localhost'
});

const start = async () => {
	await server.register([
		{
			plugin: require('./faker-plugin'),
			options: {}
		}
	]);

	await server.start();
	console.log(`Server running at: ${server.info.uri}`);
}

process.on('unhandledRejection', (err) => {
	console.log(err);
	process.exit();
});

start();
