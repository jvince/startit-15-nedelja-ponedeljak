import {
	get,
	isFinite,
	times
} from 'lodash';

import faker from 'faker';

const MIN = 1;
const MAX = 20;

const getParam = (p) => {
	if (!isFinite(parseInt(p, 10)) || p < MIN || p > MAX) {
		return MAX;
	}

	return p;
}

export const plugin = {
	name: 'fakerPlugin',
	version: '1.0.0',
	register: async (server, options) => {
		server.route({
			method: 'GET',
			path: '/people',
			options: {
				cors: true,
				jsonp: 'callback'
			},
			handler: ({ query }, h) => {
				const p = getParam(get(query, 'p'));

				return times(p, faker.helpers.contextualCard);
			}
		});
	}
};
