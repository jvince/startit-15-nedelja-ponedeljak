<?php
	include_once(__DIR__ . '/data.php');

	header('Content-Type: application/javascript');

	$scheme = empty($_SERVER['HTTPS']) ? 'http' : 'htts';
	$request_url = $scheme . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

	$url = parse_url($request_url, PHP_URL_QUERY);
	parse_str($url, $query);

	$callback = $query['callback'];
	$n = empty($query['n'])
		? 10 : $query['n'];

	$result = array_slice($data, 0, $n);

	echo $callback .'(' . json_encode($result) . ')';