<?php
	include_once(__DIR__ . '/data.php');

	// https://enable-cors.org/server_php.html
	// header("Access-Control-Allow-Origin: *");
	header('Content-Type: application/json');

	echo json_encode($data);